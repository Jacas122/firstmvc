<?php

require_once(__DIR__.'/../model/Fabric.php');
require_once(__DIR__.'/../model/db/FabricDb.php');

class IndexController{

    public function listFabrics(){
        $db = new FabricDb();
        $fabs = $db->listFabrics();
        
        return $fabs;
    }

    public function deatilsFabric($pos = 0){
       $db = new FabricDb();
       return $db->getFabric($pos);
    }

    public function createFabric($th, $dur, $col, $img){
        $db = new FabricDb();
        $thread = $db->insertFabric($th, $dur, $col, $img);
        $ext = substr($_FILES['thimg']['name'],strpos($_FILES['thimg']['name'],'.'));
        $name = $thread->getId().'-main'.$ext;
        move_uploaded_file($_FILES['thimg']['tmp_name'], UPLOAD_DIR.$name);
        return $db->updateImage($thread->getId(), $name);
     }

     public function setMainImage($fid, $img){
      $db = new FabricDb();
      return $db->insertFabric($th, $dur, $col, $img);
     }

     public function updateFabric($th, $dur, $col, $tid){
        $db = new FabricDb();
        return $db->updateRegFabric($th, $dur, $col, $tid);
     }

     public function deleteFabric($tid){
        $db = new FabricDb();
        $fab = $db->getFabric($tid);

        $db->removeFabric($tid);

        return $fab;
     }

}
