<?php

require_once(__DIR__.'/../model/db/UserDb.php');

class UserController{

    public function createUser($username, $password, $role){
        $sltedpwd = md5(SALT_BIT.$password);
        $db = new UserDb();
        return $db->insertUser($username, $sltedpwd, $role);
     }

     public function loginUser($username, $password){
        $db = new UserDb();
        $user = $db->getUserByUsername($username);

        //COMPROVAR PASSWORD
        $sltedpwd = md5(SALT_BIT.$password);
        if($sltedpwd == $user->getPwd()){
            //Login
            $_SESSION[SESS_UNAME] = $user->getUname();
            $_SESSION[SESS_ROLE] = $user->getRol();
            $db->lastLogUser($user->getUid());
            return true;
        }else{
            //No login
            return false;
        }
     }

}
