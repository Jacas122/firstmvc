<?php

class User{

    private $_uid;
    private $_pwd;
    private $_rol;
    private $_uname;
    private $_created;
    private $_lastlog;

    public function __construct($un = "", $pass = "", $rol = UROLE_USER, 
            $created = NULL, $lastlog = NULL, $uid = NULL){
        
        $this->setUname($un);
        $this->setPwd($pass);
        $this->setRol($rol);
        $this->setCreated($created);
        if($lastlog != NULL){
            $this->setLastLog($lastlog);
        }
        if($uid != NULL){
            $this->setUid($uid);
        }
    }

    public function getUid(){
        return $this->_uid;
    }
    public function getPwd(){
        return $this->_pwd;
    }
    public function getUname(){
        return $this->_uname;
    }
    public function getCreated(){
        return $this->_created;
    }
    public function getRol(){
        return $this->_rol;
    }
    public function getlastLog(){
        return $this->_lastlog;
    }

    public function setUid($v){
        $this->_uid = $v;
    }
    public function setPwd($v){
        $this->_pwd = $v;
    }
    public function setRol($v){
        $this->_rol = $v;
    }
    public function setUname($v){
        $this->_uname = $v;
    }
    public function setCreated($v){
        $this->_created = $v;
    }
    public function setLastLog($v){
        $this->_lastlog = $v;
    }

}
