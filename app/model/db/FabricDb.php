<?php

require_once(__DIR__.'/../Fabric.php');

class FabricDb{

    private $_conn;

    public function listFabrics(){
        //OPEN CONNECTION TO DATABASE
        $this->openConnection();
         
        //PREPARE STATEMENT WITH NO PARAMETERS AT THE MOMENT
        $query = "SELECT * FROM thread";
        $stmt = $this->_conn->prepare($query);
        
        //EXECUTE QUERY
        $stmt->execute();
        $res = $stmt->get_result();
        
        //RETRIEVE RESULTS AND BUILD RETURN ARRAY
        $fabs = array();
        while ($fab = $res->fetch_assoc() ) {
          array_push($fabs, new Fabric($fab['threads'], $fab['durability'],
              $fab['color'], $fab['img_filename'], $fab['idt']));
        }
        return $fabs;
      }

      public function getFabric($tid){
        //OPEN CONNECTION TO DATABASE
        $this->openConnection();

        //PREPARE STATEMENT WITH ONE PARAMETER
        $query = "SELECT * FROM thread WHERE idt = ?";
        $stmt = $this->_conn->prepare($query);

        //DEFINE PAFAMETER
        $stmt->bind_param("i", $index);
        $index = $tid;
    
        //EXECUTE QUERY
        $stmt->execute();
        $res = $stmt->get_result();
    
        //RETRIEVE RESULT AND BUILD RETURN OBJECT
        $fabres = $res->fetch_assoc();
        return new Fabric($fabres['threads'], $fabres['durability'], 
            $fabres['color'], $fabres['img_filename'], $fabres['idt']);
      }

      public function insertFabric($threads, $durability, $color, $img){
        //OPEN CONNECTION TO DATABASE
        $this->openConnection();

        //PREPARE STATEMENT WITH ONE PARAMETER
        $query = "INSERT INTO thread (threads, durability, color, img_filename) VALUES (?, ?, ?, ?)";
        $stmt = $this->_conn->prepare($query);

        //DEFINE PAFAMETER
        $stmt->bind_param("isss", $t, $d, $c, $im);
        $t = $threads;
        $d = $durability;
        $c = $color;
        $im = $img;
    
        //EXECUTE QUERY
        $stmt->execute();

        //echo('ID insert');
        //var_dump($stmt->insert_id);
    
        //RETRIEVE RESULT AND BUILD RETURN OBJECT
        return $this->getFabric($stmt->insert_id);
      }

      public function updateImage($id, $image){
        //OPEN CONNECTION TO DATABASE
        $this->openConnection();

        //PREPARE STATEMENT WITH ALL THE AVAILABLE PARAMETERS
        $query = "UPDATE thread SET img_filename = ? WHERE idt = ? ";
        $stmt = $this->_conn->prepare($query);

        //DEFINE PAFAMETER
        $stmt->bind_param("si", $img, $tid);
        $tid = $id;
        $img = $image;
    
        //EXECUTE QUERY
        $stmt->execute();
    
        //RETRIEVE RESULT AND BUILD RETURN OBJECT
        return $this->getFabric($id);
      }

      public function updateRegFabric($threads, $durability, $color, $id){
        //OPEN CONNECTION TO DATABASE
        $this->openConnection();

        //PREPARE STATEMENT WITH ALL THE AVAILABLE PARAMETERS
        $query = "UPDATE thread SET threads = ?, durability = ?, color = ? WHERE idt = ? ";
        $stmt = $this->_conn->prepare($query);

        //DEFINE PAFAMETER
        $stmt->bind_param("issi", $t, $d, $c, $i);
        $t = $threads;
        $d = $durability;
        $c = $color;
        $i = $id;
    
        //EXECUTE QUERY
        $stmt->execute();
    
        //RETRIEVE RESULT AND BUILD RETURN OBJECT
        return $this->getFabric($id);
      }

      public function removeFabric($id){
          //OPEN CONNECTION TO DATABASE
          $this->openConnection();
  
          //PREPARE STATEMENT WITH ONE PARAMETER
          $query = "DELETE FROM thread WHERE idt = ?";
          $stmt = $this->_conn->prepare($query);
  
          //DEFINE PAFAMETER
          $stmt->bind_param("i", $index);
          $index = $id;
      
          //EXECUTE QUERY
          $stmt->execute();
          return true;
      }


    private function openConnection(){
        if($this->_conn == NULL){
          $this->_conn = mysqli_connect(DB_HOST, DB_USER, DB_PWD, DB_DB);    
        }
    }

}
