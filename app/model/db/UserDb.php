<?php 

require_once(__DIR__.'/../User.php');

class UserDb{

    private $_conn;

    public function insertUser($uname, $pass, $rol){
        //OPEN CONNECTION TO DATABASE
        $this->openConnection();

        //PREPARE STATEMENT WITH ONE PARAMETER
        $query = "INSERT INTO user (username, password, user_role) VALUES (?, ?, ?)";
        $stmt = $this->_conn->prepare($query);

        //DEFINE PAFAMETER
        $stmt->bind_param("ssi", $u, $p, $ur);
        $u = $uname;
        $p = $pass;
        $ur = $rol;
    
        //EXECUTE QUERY
        $stmt->execute();

        //RETRIEVE RESULT AND BUILD RETURN OBJECT
        //return $this->getFabric($stmt->insert_id);
        return true;
      }

      public function getUserByUsername($uname){
        $this->openConnection();

        $query = "SELECT * FROM user WHERE username = ?";
        $stmt = $this->_conn->prepare($query);

        $stmt->bind_param("s", $u);
        $u = $uname;
        
        $stmt->execute();
        $res = $stmt->get_result();
        $ures = $res->fetch_assoc();

        return $this->userInstanceFromAssoc($ures);
      }

      public function lastLogUser($uid){
        $this->openConnection();

        $query = "UPDATE user set last_login = NOW() WHERE uid = ?";
        $stmt = $this->_conn->prepare($query);

        $stmt->bind_param("i", $u);
        $u = $uid;
        
        $stmt->execute();
        //FALTA CONTROL ERRORS
        return true;
      }

      private function userInstanceFromAssoc($assoc){
        return new User($assoc['username'], $assoc['password'], $assoc['user_role'],
            $assoc['date_created'], $assoc['last_login'], $assoc['uid']);
      }

    private function openConnection(){
        if($this->_conn == NULL){
          $this->_conn = mysqli_connect(DB_HOST, DB_USER, DB_PWD, DB_DB);    
        }
    }

}