<html>
  <head>
    <title>Add Fabric</title>
  </head>
  <body>
    <h1>Add fabric</h1>
    <form id="thform" method="post" action="/forms/add.php" enctype="multipart/form-data">
    <dl>
        <dt><label for="addt-threads">Threads</label></dt>
        <dd><input type="numeric" id="addt-threads" name="thth" tabindex="1"/></dd>
        <dt><label for="addt-dur">Durability</label></dt>
        <dd><input type="text" id="addt-dur" name="thdur" tabindex="2"/></dd>
        <dt><label for="addt-color">Threads</label></dt>
        <dd>
            <select id="addt-color" name="thcol" tabindex="3">
                <option value="black" selected>Black</option>
                <option value="blue">Blue</option>
                <option value="red">Red</option>
                <option value="green">Green</option>
            </select>
        </dd>
        <dt><label for="addt-img">Image</label></dt>
        <dd><input type="file" id="addt-img" name="thimg" tabindex="4"/></dd>
        <dt>&nbsp;</dt>
        <dd><input type="submit" value="Add" name="thsub"/></dd>
    </dl>
    </form>
    <a href="/">Back to home</a>
  </body>
</html>