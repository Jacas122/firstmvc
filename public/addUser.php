<?php
  require_once(__DIR__."/../app/inc/constants.php")
?><html>
  <head>
    <title>Add User</title>
  </head>
  <body>
    <h1>Add User</h1>
    <form id="uform" method="post" action="/forms/addUser.php">
    <dl>
        <dt><label for="addu-name">Username</label></dt>
        <dd><input type="text" id="addu-name" name="uname" tabindex="1"/></dd>
        <dt><label for="addu-pass">Password</label></dt>
        <dd><input type="text" id="addu-pass" name="upass" tabindex="2"/></dd>
        <dt><label for="addu-rol">Rol</label></dt>
        <dd>
            <select id="addu-rol" name="urol" tabindex="3">
                <option value="<?=UROLE_USER?>" selected>Usuari</option>
                <option value="<?=UROLE_ADMIN?>">Administrador</option>
            </select>
        </dd>
        <dt>&nbsp;</dt>
        <dd><input type="submit" value="Add User" name="usub"/></dd>
    </dl>
    </form>
    <a href="/">Back to home</a>
  </body>
</html>