<?php

require_once(__DIR__.'/../../app/inc/constants.php');
require_once(__DIR__.'/../../app/controller/IndexController.php');

//RECUPEREM DADES
$id = $_GET['index'];

$cnt = new IndexController();
$fs = $cnt->deleteFabric($id);

?><html>
  <head>
    <title>Deleted Fabric</title>
  </head>
  <body>
    <h1>Successfully deleted</h1>
    <hr style="color:<?=$fs->getColor()?>;"/>
    <ul>
      <li><?=$fs->getThreads()?></li>
      <li><?=$fs->getColor()?></li>
      <li><?=$fs->getDurability()?></li>
    </ul>
    <a href="/">Back to home</a>
  </body>
</html>