<?php

require_once(__DIR__.'/../app/inc/constants.php');
require_once(__DIR__.'/../app/controller/IndexController.php');

$cnt = new IndexController();
$fs = $cnt->deatilsFabric($_GET['index']);

?><html>
<head>
  <title>Update Fabric</title>
</head>
<body>
  <h1>Update fabric</h1>
  <form id="thform" method="post" action="/forms/update.php">
  <dl>
      <dt><label for="addt-threads">Threads</label></dt>
      <dd><input type="numeric" id="addt-threads" name="thth" tabindex="1" value="<?=$fs->getThreads()?>"/></dd>
      <dt><label for="addt-dur">Durability</label></dt>
      <dd><input type="text" id="addt-dur" name="thdur" tabindex="2" value="<?=$fs->getDurability()?>"/></dd>
      <dt><label for="addt-color">Color</label></dt>
      <dd>
          <select id="addt-color" name="thcol" tabindex="3">
              <option value="black"<?php if($fs->getColor() == "black"){ echo " selected";} ?>>Black</option>
              <option value="blue"<?php if($fs->getColor() == "blue"){ echo " selected";} ?>>Blue</option>
              <option value="red"<?php if($fs->getColor() == "red"){ echo " selected";} ?>>Red</option>
              <option value="green"<?php if($fs->getColor() == "green"){ echo " selected";} ?>>Green</option>
          </select>
      </dd>
      <dt><input type="hidden" name="thid" value="<?=$fs->getId()?>"/></dt>
      <dd><input type="submit" value="Update" name="thsub"/></dd>
  </dl>
  </form>
  <a href="/">Back to home</a>
</body>
</html>